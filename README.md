# Raspi wifi router & more

1. Install [RaspAP](https://raspap.com)

Documentation [here](https://docs.raspap.com/quick/#usage)

```bash
apt update && apt full-upgrade -y           # update the board
sudo raspi-config                           # setup the wifi location (if not setup when flashing the sd card)
curl -sL https://install.raspap.com | bash -s -- --yes --wireguard 1 --openvpn 1 --adblock 1 --minwrite --certificate
```

defaults cretantial are:
`admin` `secret`

2. Install [Docker](https://www.docker.com)

Documentation [here](https://docs.docker.com/engine/install/raspberry-pi-os/)

```bash
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done

sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

3. Add some servicies

- print server (CUPS) *[docker image](https://hub.docker.com/r/olbat/cupsd)*
- file sharing server (FTP) *[docker image](https://hub.docker.com/r/drakkan/sftpgo)*
- file sharing server (SAMBA) *[docker image](https://hub.docker.com/r/mekayelanik/samba-server-alpine)*
- webgui for docker (Portainer) *[docker image](https://hub.docker.com/r/portainer/portainer-ce)*
- Wake on Lan (upsnap) *[docker image](https://github.com/seriousm4x/UpSnap)*

4. Prepare backup

**Automount a usbkey**

create a udev rule (in `/etc/udev/rule.d/`) named `10-usb-backup-automount.rules`

```bash
ACTION=="add", KERNEL=="sd.*", SUBSYSTEMS=="usb", ENV{ID_FS_TYPE}=="ntfs", RUN+="/bin/mount %k1 /media/usb-backup && <backup script>"
```

**Backup on usbkey or "cloud"**

use borgbackup
```bash
sudo apt install borgbackup -y
```

backup on usb:
```bash
borg create /media/usb-backup::$(date +%d%m%Y) /etc/hostapd /var/lib/docker/volumes
```

backup on distant server:
```bash
borg create ssh://<user>@<ip>:/path/on/server::$(date +%d%m%Y) /etc/hostapd /var/lib/docker/volumes
```

